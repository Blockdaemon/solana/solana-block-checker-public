package config

import (
	"fmt"

	"github.com/tkanos/gonfig"
)

type Configuration struct {
	RPC_ENDPOINT string
	RPC_PASSWORD string
}

// GetConfig returns the configuration for the given environment.
func GetConfig(params ...string) Configuration {
	configuration := Configuration{}
	env := "dev"
	if len(params) > 0 {
		env = params[0]
	}
	fileName := fmt.Sprintf("./%s_config.json", env)
	err := gonfig.GetConf(fileName, &configuration)
	if err != nil {
		return Configuration{}
	}
	return configuration
}
