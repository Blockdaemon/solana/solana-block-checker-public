package rpc

import (
	"context"
	"github.com/avast/retry-go"
	"github.com/gagliardetto/solana-go"
	solanaRpc "github.com/gagliardetto/solana-go/rpc"
	"github.com/pterm/pterm"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/config"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/data"
	"log"
	"time"
)

type Rpc struct {
	client    *solanaRpc.Client
	endpoint  string
	authToken string
}

// NewClient Create a new RPC client.
func NewClient() *Rpc {
	configuration := config.GetConfig("prod")

	rpcHost := "http://127.0.0.1:8899"
	if configuration.RPC_ENDPOINT != "" {
		rpcHost = configuration.RPC_ENDPOINT
	}
	rpcToken := ""
	if configuration.RPC_PASSWORD != "" {
		rpcToken = configuration.RPC_PASSWORD
	}
	rpcClient := Rpc{
		endpoint:  rpcHost + "?auth=" + rpcToken,
		authToken: rpcToken,
	}

	pterm.Info.Println("Connecting to rpcClient:", rpcClient.endpoint)
	rpcClient.client = solanaRpc.New(rpcClient.endpoint)

	return &rpcClient
}

// GetBlock returns the block data for the given slot.
func (rpc *Rpc) GetBlock(slotNumber uint64) (data.SlotData, error) {
	var block data.SlotData
	var out *solanaRpc.GetBlockResult
	includeRewards := false
	err := retry.Do(
		func() error {
			var err error
			out, err = rpc.client.GetBlockWithOpts(
				context.Background(),
				slotNumber,
				// You can specify more options here:
				&solanaRpc.GetBlockOpts{
					Encoding:           solana.EncodingBase64,
					Commitment:         solanaRpc.CommitmentFinalized,
					TransactionDetails: solanaRpc.TransactionDetailsNone,
					Rewards:            &includeRewards,
				},
			)
			return err
		},
		retry.Attempts(100),
		retry.Delay(1*time.Minute),
		retry.OnRetry(func(n uint, err error) {
			log.Printf("RPC request GetBlock failed: %v", err)
		}),
	)
	if err != nil {
		panic(err)
	}
	if err != nil {
		block.BlockHeight = 0
		return block, err
	}
	block.BlockHeight = *out.BlockHeight

	return block, nil
}

// GetFirstBlock GetBlock returns first available block
func (rpc *Rpc) GetFirstBlock() uint64 {
	out := uint64(0)
	err := retry.Do(
		func() error {
			var err error
			out, err = rpc.client.GetFirstAvailableBlock(context.Background())
			return err
		},
		retry.Attempts(100),
		retry.Delay(1*time.Minute),
		retry.OnRetry(func(n uint, err error) {
			log.Printf("RPC request GetFirstBlock failed: %v", err)
		}),
	)
	if err != nil {
		panic(err)
	}
	return out
}

// GetSlot returns the current slot.
func (rpc *Rpc) GetSlot() uint64 {
	out := uint64(0)
	err := retry.Do(
		func() error {
			var err error
			out, err = rpc.client.GetSlot(
				context.Background(),
				solanaRpc.CommitmentFinalized,
			)
			return err
		},
		retry.Attempts(100),
		retry.Delay(1*time.Minute),
		retry.OnRetry(func(n uint, err error) {
			log.Printf("RPC request GetSlot failed: %v", err)
		}),
	)
	if err != nil {
		panic(err)
	}
	return out
}
