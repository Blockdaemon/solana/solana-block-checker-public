package api

import (
	"context"
	"errors"
	"net/http"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files/v2"
	ginSwagger "github.com/swaggo/gin-swagger"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/docs"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/bigtable"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/logger"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/sql"
	"go.uber.org/atomic"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

// @title           Solana Bigtable Checker
// @version         1.0
// @description     Monitor for Solana full history on Bigtable
// @termsOfService  https://www.blockdaemon.com

// @contact.name   Blockdaemon
// @contact.url    https://www.blockdaemon.com
// @contact.email  support@blockdaemon.com

// @host      localhost:8403
// @BasePath  /v1
// @schemes http

const apiPort = "8403"

type BlockCheckerIntf interface {
	Db() *sql.DatabaseClient
	AddScanRange(from uint64, till uint64)
}

type GetBlocksResponse interface { // Dummy
	*sql.GetSlotsResponse
}

type Api struct {
	Server   *gin.Engine
	http     *http.Server
	group    *errgroup.Group
	log      zap.Logger
	ReqCount atomic.Uint64
}

// NewApi creates a new API server.
func NewApi(ctx context.Context) *Api {
	gin.SetMode(gin.ReleaseMode)
	log := logger.GetLogger()
	server := gin.New()
	apiClient := Api{
		Server: server,
		log:    *log.Named("api"),
	}
	httpLog := log.Named("http")
	server.Use(ginzap.Ginzap(httpLog, time.RFC3339, true))
	server.Use(ginzap.RecoveryWithZap(httpLog, false))

	// Start services.
	apiClient.group, ctx = errgroup.WithContext(ctx)

	httpLog.Info("Starting api server", zap.String("listen", apiPort))

	// Optionally set Swagger info
	docs.SwaggerInfo.BasePath = "/v1"

	server.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	return &apiClient
}

// Serve starts the API server.
func (client *Api) Serve(checker BlockCheckerIntf) {
	ctx := context.Background()

	group := client.Server.Group("/v1")
	group.Use(client.ReqCounter())

	handler := NewHandler(checker)
	handler.RegisterHandlers(group)

	client.group.Go(func() error {
		client.http = &http.Server{
			Addr:    ":" + apiPort, // TODO check if this works right
			Handler: client.Server,
		}
		go func() {
			<-ctx.Done()
			_ = client.http.Close()
		}()
		if err := client.http.ListenAndServe(); errors.Is(err, http.ErrServerClosed) {
			return nil
		} else {
			return err
		}
	})

	// Wait until crash or graceful exit.
	if err := client.group.Wait(); err != nil {
		client.log.Error("Crashed", zap.Error(err))
	} else {
		client.log.Info("Shutting down")
	}
}

// Handler implements the tracker API methods.
type Handler struct {
	checker  BlockCheckerIntf
	dbClient *sql.DatabaseClient
}

// NewHandler creates a new tracker API using the provided database.
func NewHandler(checker BlockCheckerIntf) *Handler {
	return &Handler{
		checker:  checker,
		dbClient: checker.Db(),
	}
}

// ReqCounter counts the number of requests.
func (client *Api) ReqCounter() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()
		client.ReqCount.Add(1)
	}
}

// RegisterHandlers registers this API with Gin web framework.
func (h *Handler) RegisterHandlers(group gin.IRoutes) {
	group.GET("/state", h.AppState)
	group.GET("/missing", h.GetMissing)
	group.GET("/available", h.GetAvailable)
	group.GET("/unknown", h.GetUnknown)
	group.GET("/skipped", h.GetSkipped)
	group.GET("/unrecoverable", h.GetUnrecoverable)
	group.GET("/slots", h.GetSlots)
	group.GET("/slot-parent", h.GetParentSlot)
	group.GET("/block-by-slot", h.GetBlockBySlot)
	group.GET("/blockheight", h.GetBlockHeight)
	group.GET("/missing-blocks-count", h.GetCountMissingBlocks)
	group.POST("/rescan", h.Rescan)
	group.POST("/update", h.Update)
}

// AppState Update godoc
// @Summary Get info if app is online
// @Description Get info if app is online
// @Tags app
// @Accept json
// @Produce json
// @Success 201
// @Router /state [get]
func (h *Handler) AppState(c *gin.Context) {
	var state = "online"
	c.JSON(http.StatusOK, state)
}

// GetMissing godoc
// @Summary Get missing slots
// @Description Get the slotnumber for all missing
// @Tags slots
// @Accept json
// @Produce json
// @Param   limit         query     uint64        false  "int valid"          minimum(1)
// @Success 200 {object} object{data=[]int} "List of missing slots"
// @Router /missing [get]
func (h *Handler) GetMissing(c *gin.Context) {
	var query struct {
		Limit int `form:"limit"`
	}
	if err := c.BindQuery(&query); err != nil {
		return
	}

	c.JSON(http.StatusOK, h.dbClient.GetSlotsMissing(query.Limit))
}

// GetAvailable godoc
// @Summary Get available slots
// @Description Get the slotnumber for all available
// @Tags slots
// @Accept json
// @Produce json
// @Param   limit         query     uint64        false  "int valid"          minimum(1)
// @Success 200 {object} object{data=[]int} "List of available slots"
// @Router /available [get]
func (h *Handler) GetAvailable(c *gin.Context) {
	var query struct {
		Limit int `form:"limit"`
	}
	if err := c.BindQuery(&query); err != nil {
		return
	}
	c.JSON(http.StatusOK, h.dbClient.GetSlotsAvailable(query.Limit))
}

// GetSkipped godoc
// @Summary Get skipped slots
// @Description Get the slotnumber for all skipped
// @Tags slots
// @Accept json
// @Produce json
// @Param   limit         query     uint64        false  "int valid"          minimum(1)
// @Success 200 {object} object{data=[]int} "List of skipped slots"
// @Router /skipped [get]
func (h *Handler) GetSkipped(c *gin.Context) {
	var query struct {
		Limit int `form:"limit"`
	}
	if err := c.BindQuery(&query); err != nil {
		return
	}
	c.JSON(http.StatusOK, h.dbClient.GetSlotsSkipped(query.Limit))
}

// GetUnrecoverable godoc
// @Summary Get unrecoverable slots
// @Description Get the slotnumber for all unrecoverable
// @Tags slots
// @Accept json
// @Produce json
// @Param   limit         query     uint64        false  "int valid"          minimum(1)
// @Success 200 {object} object{data=[]int} "List of unrecoverable slots"
// @Router /unrecoverable [get]
func (h *Handler) GetUnrecoverable(c *gin.Context) {
	var query struct {
		Limit int `form:"limit"`
	}
	if err := c.BindQuery(&query); err != nil {
		return
	}
	c.JSON(http.StatusOK, h.dbClient.GetSlotsUnrecoverable(query.Limit))
}

// GetUnknown godoc
// @Summary Get unknown slots
// @Description Get the slotnumber for all unknown missing
// @Tags slots
// @Accept json
// @Produce json
// @Param   limit         query     uint64        false  "int valid"          minimum(1)
// @Success 200 {object} object{data=[]int} "List of unknown slots"
// @Router /unknown [get]
func (h *Handler) GetUnknown(c *gin.Context) {
	var query struct {
		Limit int `form:"limit"`
	}
	if err := c.BindQuery(&query); err != nil {
		return
	}
	c.JSON(http.StatusOK, h.dbClient.GetSlotsUnknown(query.Limit))
}

// GetSlots godoc
// @Summary Get all slots
// @Description Get the slotnumber for all missing
// @Tags slots
// @Accept json
// @Produce json
// @Success 200 {object} object{data=[]GetSlotsResponse} "List of slots and state"
// @Router /slots [get]
func (h *Handler) GetSlots(c *gin.Context) {
	c.JSON(http.StatusOK, h.dbClient.GetSlots())
}

// Update godoc
// @Summary Update slot state
// @Description Update the slot state to missing or available
// @Tags slots
// @Accept json
// @Produce json
// @Param   form         query     uint64        false  "int valid"          minimum(1)
// @Param   state         query     string        false  ""          minimum(1)
// @Success 201
// @Router /update [post]
func (h *Handler) Update(c *gin.Context) {
	var query struct {
		Slot  uint64 `form:"slot"`
		State string `form:"state"`
	}
	if err := c.BindQuery(&query); err != nil {
		return
	}

	var state = bigtable.SlotStateFromString(query.State)

	h.dbClient.UpsertSlotState(query.Slot, state)

	c.JSON(http.StatusCreated, struct{}{})
}

// Rescan godoc
// @Summary (Re-)scan slot range for missing slots
// @Description Scan the range for change slot states, like missing slots
// @Tags slots
// @Accept json
// @Produce json
// @Param   from         query     uint64        false  "int valid"          minimum(1)
// @Param   till         query     uint64        false  "int valid"          minimum(1)
// @Success 201
// @Router /rescan [post]
func (h *Handler) Rescan(c *gin.Context) {
	var query struct {
		From uint64 `form:"from"`
		Till uint64 `form:"till"`
	}
	if err := c.BindQuery(&query); err != nil {
		return
	}
	if query.From < 0 {
		query.From = 0
	}
	if query.From > query.Till {
		query.From = query.Till
	}

	h.checker.AddScanRange(query.From, query.Till)

	c.JSON(http.StatusCreated, struct{}{})
}

// GetParentSlot Get parent slot number
// @Summary Get parent slot
// @Description Get the parent slot number for a given slot
// @Tags slots
// @Accept json
// @Produce json
// @Param   slot         query     uint64        false  "int valid"          minimum(1)
// @Success 200
// @Router /parent [get]
func (h *Handler) GetParentSlot(c *gin.Context) {
	var query struct {
		Slot int `form:"slot"`
	}
	if err := c.BindQuery(&query); err != nil {
		return
	}
	if query.Slot < 0 {
		query.Slot = 0
	}

	c.JSON(http.StatusOK, h.dbClient.GetParentSlot(uint64(query.Slot)))
}

// GetBlockBySlot Get block height by slot number
// @Summary Get block height by slot
// @Description Get the block height for a given slot
// @Tags blocks
// @Accept json
// @Produce json
// @Param   slot         query     uint64        false  "int valid"          minimum(1)
// @Success 200
// @Router /slot-height [get]
func (h *Handler) GetBlockBySlot(c *gin.Context) {
	var query struct {
		Slot int `form:"slot"`
	}
	if err := c.BindQuery(&query); err != nil {
		return
	}
	if query.Slot < 0 {
		query.Slot = 0
	}

	c.JSON(http.StatusOK, h.dbClient.GetBlockBySlot(uint64(query.Slot)))
}

// GetBlockHeight Get max block height
// @Summary Get max block height
// @Description Get max the block height
// @Tags blocks
// @Accept json
// @Produce json
// @Success 200
// @Router /height [get]
func (h *Handler) GetBlockHeight(c *gin.Context) {
	c.JSON(http.StatusOK, h.dbClient.GetBlockHeight())
}

// GetCountMissingBlocks Get count of missing blocks
// @Summary Get count of missing blocks
// @Description Get the count of missing blocks
// @Tags blocks
// @Accept json
// @Produce json
// @Success 200
// @Router /missing-blocks/count [get]
func (h *Handler) GetCountMissingBlocks(c *gin.Context) {
	c.JSON(http.StatusOK, h.dbClient.GetCountMissingBlocks())
}
