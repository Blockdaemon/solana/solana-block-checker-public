package data

type SlotData struct {
	Slot        uint64
	BlockHeight uint64
	Missing     bool
	ErrorCode   int
}
