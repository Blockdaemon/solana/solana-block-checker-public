package sql

import (
	dbsql "database/sql"
	"fmt"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/bigtable"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/data"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/logger"
	"go.uber.org/zap"
	"strconv"
	"strings"
	"sync"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

const (
	databasefile string = "data/solana-history.db"
	create       string = `
CREATE TABLE IF NOT EXISTS solanahistory (
	slot INTEGER NOT NULL PRIMARY KEY,
	parent INTEGER,
	block INTEGER,
	state INTEGER NOT NULL
  );`
)

type UpdateBuffers struct {
	slotState  *sync.Map
	slotParent *sync.Map
	slotBlock  *sync.Map
}

type DatabaseClient struct {
	database     *dbsql.DB
	updateBuffer *UpdateBuffers
	queryLock    *sync.Mutex
	bufferLock   *sync.Mutex
	log          *zap.Logger
}

// NewDatabaseClient Create a new database client
func NewDatabaseClient() *DatabaseClient {
	log := logger.GetLogger()

	db, err := dbsql.Open("sqlite3", databasefile)
	log.Info("Created database file")

	if err != nil {
		log.Panic(err.Error())
		panic(err)
	}

	client := DatabaseClient{
		database: db,
		updateBuffer: &UpdateBuffers{
			slotState:  &sync.Map{},
			slotParent: &sync.Map{},
			slotBlock:  &sync.Map{},
		},
		log:        log,
		queryLock:  &sync.Mutex{},
		bufferLock: &sync.Mutex{},
	}

	// Create table if it doesn't exist
	if _, err := db.Exec(create); err != nil {
		log.Panic(err.Error())
		panic(err)
	}
	log.Info("Database initialized")

	return &client
}

// UpsertSlotState Collect all state updates to write at once
func (client *DatabaseClient) UpsertSlotState(slotNumber uint64, state bigtable.SlotState) {
	client.bufferLock.Lock()
	defer client.bufferLock.Unlock()
	client.updateBuffer.slotState.Store(slotNumber, state)
}

// UpsertSlotParent Collect all parent updates to write at once
func (client *DatabaseClient) UpsertSlotParent(slotNumber uint64, parentSlot uint64) {
	client.bufferLock.Lock()
	defer client.bufferLock.Unlock()
	client.updateBuffer.slotParent.Store(slotNumber, parentSlot)
}

// UpsertSlotBlock Collect all blockheight updates to write at once
func (client *DatabaseClient) UpsertSlotBlock(slotNumber uint64, blockheight uint64) {
	client.bufferLock.Lock()
	defer client.bufferLock.Unlock()
	client.updateBuffer.slotBlock.Store(slotNumber, blockheight)
}

// Write all updates to database
func (client *DatabaseClient) flush() {
	client.queryLock.Lock()
	defer client.queryLock.Unlock()
	client.bufferLock.Lock()
	defer client.bufferLock.Unlock()

	client.log.Info("Flushing slots to database")

	client.flushBuffer(
		client.updateBuffer.slotBlock,
		"INSERT INTO solanahistory (slot,block,state) VALUES %s ON CONFLICT (slot) DO UPDATE SET block=excluded.block",
		func(key any, value any) string {
			slotNr := key.(uint64)
			height := value.(uint64)

			return "(" + strconv.Itoa(int(slotNr)) + "," + strconv.Itoa(int(height)) + ", " + strconv.Itoa(int(bigtable.Unknown)) + ")"
		},
	)

	client.flushBuffer(
		client.updateBuffer.slotParent,
		"INSERT INTO solanahistory (slot,parent,state) VALUES %s ON CONFLICT (slot) DO UPDATE SET parent=excluded.parent",
		func(key any, value any) string {
			slotNr := key.(uint64)
			parentSlot := value.(uint64)

			return "(" + strconv.Itoa(int(slotNr)) + "," + strconv.Itoa(int(parentSlot)) + ", " + strconv.Itoa(int(bigtable.Unknown)) + ")"
		},
	)

	client.flushBuffer(
		client.updateBuffer.slotState,
		"INSERT INTO solanahistory (slot,state) VALUES %s ON CONFLICT (slot) DO UPDATE SET state=excluded.state",
		func(key any, value any) string {
			slotNr := key.(uint64)
			state := value.(bigtable.SlotState)

			return "(" + strconv.Itoa(int(slotNr)) + "," + strconv.Itoa(int(state)) + ")"
		},
	)
}

// Write buffer to database
func (client *DatabaseClient) flushBuffer(buffer *sync.Map, queryFormat string, serializeValues func(key any, values any) string) {
	var values []string
	// Iterate over all values in the buffer
	buffer.Range(func(key any, value any) bool {
		fieldValues := serializeValues(key, value)

		// Add to the list of values to write
		values = append(values, fieldValues)

		return true
	})

	client.log.Info(fmt.Sprintf("%v slots in query", len(values)))

	if len(values) > 0 {
		// Write all values to database
		query := fmt.Sprintf(queryFormat, strings.Join(values, ","))

		_, err := client.database.Exec(query)
		if err != nil {
			client.log.Panic(err.Error())
			panic(err)
		} else {
			client.log.Info("Slots inserted/updated")
			count := 0

			// Remove all values from buffer
			buffer.Range(func(key any, value any) bool {
				buffer.Delete(key)
				count++
				return true
			})
			client.log.Info(fmt.Sprintf("Deleted %v values from updatebuffer", len(values)))
		}
	}
}

// Poll Periodically flush updates to database
func (client *DatabaseClient) Poll() {
	for {
		client.flush()
		time.Sleep(5 * time.Second)
	}
}

// Search for a slot in the database
func (client *DatabaseClient) Search(query string) data.SlotData {
	client.queryLock.Lock()
	defer client.queryLock.Unlock()
	rows, _ := client.database.Query(query)

	var searchData data.SlotData
	searchData.Slot = 0

	if rows != nil {
		for rows.Next() {
			err := rows.Scan(&searchData.Slot)
			if err != nil {
				client.log.Panic(err.Error())
				panic(err)
			}
		}
	}
	return searchData
}

func (client *DatabaseClient) CountSlots() int {
	var output int
	client.queryLock.Lock()
	defer client.queryLock.Unlock()
	query, err := client.database.Prepare("SELECT COUNT(*) as count FROM solanahistory")

	if err != nil {
		client.log.Panic(err.Error())
		panic(err)
	}

	err = query.QueryRow().Scan(&output)
	if err != nil {
		client.log.Panic(err.Error())
		panic(err)
	}

	return output
}

// CountAvailable Count available slots
func (client *DatabaseClient) CountAvailable() int {
	return client.CountByState(bigtable.Available)
}

// CountUnknown Count skipped slots
func (client *DatabaseClient) CountUnknown() int {
	return client.CountByState(bigtable.Unknown)
}

// CountMissing Count missing slots
func (client *DatabaseClient) CountMissing() int {
	return client.CountByState(bigtable.Missing)
}

// CountSkipped Count missing slots
func (client *DatabaseClient) CountSkipped() int {
	return client.CountByState(bigtable.Skipped)
}

// CountUnrecoverable Count unrecoverable slots
func (client *DatabaseClient) CountUnrecoverable() int {
	return client.CountByState(bigtable.Unrecoverable)
}

// CountByState Count slots by state
func (client *DatabaseClient) CountByState(state bigtable.SlotState) int {
	var output int
	query, err := client.database.Prepare("SELECT COUNT(*) as count FROM solanahistory WHERE state=" + strconv.Itoa(int(state)))
	if err != nil {
		panic(fmt.Errorf("%s", err))
	}

	client.queryLock.Lock()
	defer client.queryLock.Unlock()
	err = query.QueryRow().Scan(&output)
	if err != nil {
		panic(err)
	}

	return output
}

// MaxSlot Get maximum slot number
func (client *DatabaseClient) MaxSlot() uint64 {
	dbLast := client.Search("SELECT slot FROM solanahistory ORDER BY ROWID DESC LIMIT 1")

	return dbLast.Slot
}

// CountMissingBlocks Get maximum slot number
func (client *DatabaseClient) CountMissingBlocks() uint64 {
	dbLast := client.Search("SELECT MAX(block) - MIN(block) - COUNT(*) FROM solanahistory WHERE state=0")

	return dbLast.Slot
}

// LastSlot Get last slot number (??)
func (client *DatabaseClient) LastSlot() uint64 {
	dbMaxSlot := client.MaxSlot()

	var firstSlot uint64
	if dbMaxSlot == 0 {
		firstSlot = 0
	} else {
		client.log.Info(fmt.Sprintf("Last slot from DB %v", dbMaxSlot))
		firstSlot = dbMaxSlot + 1
	}
	return firstSlot
}

// GetSlotsMissing Get missing slots data
func (client *DatabaseClient) GetSlotsMissing(limit int) []uint64 {
	return client.GetSlotsByState(bigtable.Missing, limit)
}

// GetSlotsAvailable Get available slots data
func (client *DatabaseClient) GetSlotsAvailable(limit int) []uint64 {
	return client.GetSlotsByState(bigtable.Available, limit)
}

// GetSlotsUnknown Get unknown slots data
func (client *DatabaseClient) GetSlotsUnknown(limit int) []uint64 {
	return client.GetSlotsByState(bigtable.Unknown, limit)
}

// GetSlotsUnrecoverable Get unrecoverable slots data
func (client *DatabaseClient) GetSlotsUnrecoverable(limit int) []uint64 {
	return client.GetSlotsByState(bigtable.Unrecoverable, limit)
}

// GetSlotsSkipped Get unknown slots data
func (client *DatabaseClient) GetSlotsSkipped(limit int) []uint64 {
	return client.GetSlotsByState(bigtable.Skipped, limit)
}

// GetSlotsByState Get slots by state
func (client *DatabaseClient) GetSlotsByState(state bigtable.SlotState, limit int) []uint64 {
	client.queryLock.Lock()
	defer client.queryLock.Unlock()

	query := "SELECT slot FROM solanahistory WHERE state=?"
	if limit > 0 {
		query = query + " LIMIT ?"
	}
	result, err := client.database.Query(query, strconv.Itoa(int(state)), limit)
	if err != nil {
		client.log.Panic(err.Error())
		panic(err)
	}

	slots := []uint64{}
	for result.Next() {
		slotNumber := uint64(0) //
		err = result.Scan(&slotNumber)
		if err != nil {
			client.log.Panic(err.Error())
			panic(err)
		}
		slots = append(slots, slotNumber)
	}

	return slots
}

// GetSlotsResponse Get slots from database response
type GetSlotsResponse struct {
	Slot   int `json:"slot"`
	Parent int `json:"parent"`
	Block  int `json:"block"`
	State  int `json:"state"`
}

// GetSlots Get slots data
func (client *DatabaseClient) GetSlots() []GetSlotsResponse {
	client.queryLock.Lock()
	defer client.queryLock.Unlock()

	result, err := client.database.Query("SELECT slot, parent, block, state FROM solanahistory")
	if err != nil {
		client.log.Panic(err.Error())
		panic(err)
	}

	var slots []GetSlotsResponse
	for result.Next() {
		slot := GetSlotsResponse{}
		err = result.Scan(&slot.Slot, &slot.Parent, &slot.Block, &slot.State)
		if err != nil {
			client.log.Panic(err.Error())
			panic(err)
		}
		slots = append(slots, slot)
	}

	return slots
}

// GetParentSlot Get parent by slot
func (client *DatabaseClient) GetParentSlot(slot uint64) uint64 {
	dbParent := client.Search("SELECT parent FROM solanahistory WHERE slot=" + strconv.Itoa(int(slot)))

	return dbParent.Slot
}

// GetBlockHeight Get max block height
func (client *DatabaseClient) GetBlockHeight() uint64 {
	dbBlock := client.Search("SELECT MAX(block) AS Slot FROM solanahistory")

	return dbBlock.Slot
}

// GetCountMissingBlocks Get count of missing blocks
func (client *DatabaseClient) GetCountMissingBlocks() uint64 {
	dbCount := client.Search("SELECT MAX(block) - MIN(block) - COUNT(*) as CountMissing FROM solanahistory WHERE state=0")

	return dbCount.Slot
}

// GetBlockBySlot Get block by slot number
func (client *DatabaseClient) GetBlockBySlot(slot uint64) uint64 {
	dbBlock := client.Search("SELECT block FROM solanahistory WHERE slot=" + strconv.Itoa(int(slot)))

	return dbBlock.Slot
}
