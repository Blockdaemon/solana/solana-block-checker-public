package bigtable

import (
	"bytes"
	"compress/bzip2"
	"compress/gzip"
	"context"
	"fmt"
	"github.com/klauspost/compress/zstd"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/logger"
	"io"
	"io/ioutil"

	"cloud.google.com/go/bigtable"
	"google.golang.org/api/option"
)

const (
	CompressionHeaderSize = 4
)

type BigtableCompression uint

const (
	bigtableCompressionNone BigtableCompression = iota
	bigtableCompressionBzip2
	bigtableCompressionGzip
	bigtableCompressionZstd
)

type SlotState uint

const (
	Available SlotState = iota
	Skipped
	Missing
	Unknown
	Unrecoverable
)

func (state SlotState) String() string {
	switch state {
	case Available:
		return "available"
	case Skipped:
		return "skipped"
	case Missing:
		return "missing"
	case Unknown:
		return "unknown"
	case Unrecoverable:
		return "unrecoverable"
	default:
		return "not checked"
	}
}

func SlotStateFromString(state string) SlotState {
	switch state {
	case "available":
		return Available
	case "skipped":
		return Available
	case "missing":
		return Missing
	case "unrecoverable":
		return Unrecoverable
	}

	return Unknown
}

type BigtableClient struct {
	project  string
	instance string
	context  context.Context
	Client   *bigtable.Client
}

// NewBigtableClient creates a new bigtable client.
func NewBigtableClient(projectName string, instanceName string) *BigtableClient {
	log := logger.GetLogger()

	c := BigtableClient{
		project:  projectName,
		instance: instanceName,
		context:  context.Background(),
	}
	log.Info(fmt.Sprintf("Connecting to big table: %s instance %s", projectName, instanceName))
	nativeClient, err := bigtable.NewClient(c.context, c.project, c.instance, option.WithCredentialsFile("credentials.json"))
	if err != nil {
		panic(fmt.Errorf("could not create data operations client: %v", err))
	}
	c.Client = nativeClient

	return &c
}

func (c BigtableCompression) String() string {
	switch c {
	case bigtableCompressionBzip2:
		return "bzip2"
	case bigtableCompressionGzip:
		return "gzip"
	case bigtableCompressionZstd:
		return "zstd"
	default:
		return "none"
	}
}

// Uncompress uncompresses block data from bigtable using the right compression algorithm.
func (c BigtableCompression) Uncompress(b []byte) ([]byte, error) {
	r := bytes.NewReader(b)
	var o io.Reader
	var err error

	switch c {
	case bigtableCompressionBzip2:
		o = bzip2.NewReader(r)
	case bigtableCompressionGzip:
		o, err = gzip.NewReader(r)
	case bigtableCompressionZstd:
		o, err = zstd.NewReader(r)
	case bigtableCompressionNone:
		o = bytes.NewReader(b)
	default:
		return nil, fmt.Errorf("unknown compression type: %d", c)
	}
	if err != nil {
		return nil, err
	}

	return ioutil.ReadAll(o)
}

// SlotKey returns the slot hexadecimal key for bigtable.
func SlotKey(slot uint64) string {
	return fmt.Sprintf("%016x", slot)
}
