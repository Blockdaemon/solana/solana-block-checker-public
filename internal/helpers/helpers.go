package helpers

import (
	"encoding/hex"
	"fmt"
	"golang.org/x/exp/constraints"
	"io"
	"os/exec"
	"strings"
)

type SlotRange struct {
	Start uint64
	End   uint64
}

// ExternalBinToProto decodes the bigtable data into a protobuf representing the block.
func ExternalBinToProto(in []byte, command string, args ...string) ([]byte, error) {
	cmd := exec.Command(command, args...)
	stdin, err := cmd.StdinPipe()
	if err != nil {
		panic(fmt.Errorf("failed to run command: %w", err))
	}

	inString := hex.EncodeToString(in)
	go func() {
		defer stdin.Close()
		_, err := io.WriteString(stdin, inString)
		if err != nil {
			return
		}
	}()

	outCntHex, err := cmd.Output()
	if err != nil {
		panic(fmt.Errorf("failed get command output: %w", err))
	}
	outHex := string(outCntHex)
	if strings.HasPrefix(outHex, "0x") {
		outHex = outHex[2:]
	}
	outHex = strings.TrimRight(outHex, "\n")
	cnt, err := hex.DecodeString(outHex)
	if err != nil {
		panic(fmt.Errorf("failed to decode output string %q: %w", outHex, err))
	}
	return cnt, nil
}

// Includes returns true if the slot is included in the range.
func (r SlotRange) Includes(slot uint64) bool {
	return r.Start <= slot && r.End >= slot
}

// Min returns the minimum of two values.
func Min[T constraints.Ordered](a, b T) T {
	if a < b {
		return a
	}
	return b
}

// Max returns the maximum of two values.
func Max[T constraints.Ordered](a, b T) T {
	if a > b {
		return a
	}
	return b
}
