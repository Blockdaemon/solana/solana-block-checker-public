package app

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

type PromServer struct {
	// PROCESS
	ProcChunkSize     prometheus.Gauge
	ProcNumDecoders   prometheus.Gauge
	ProcNumProcessors prometheus.Gauge
	ProcRunningSince  prometheus.Gauge
	ProcNumQueried    prometheus.Gauge
	ProcNumDecoded    prometheus.Gauge
	ProcNumScanned    prometheus.Gauge
	ProcNumMissed     prometheus.Gauge
	ProcNumUnknown    prometheus.Gauge
	ProcNumAvailable  prometheus.Gauge

	// DATABASE
	DbMaxSlot      prometheus.Gauge
	DbNumSlots     prometheus.Gauge
	DbNumMissed    prometheus.Gauge
	DbNumUnknown   prometheus.Gauge
	DbNumAvailable prometheus.Gauge

	// RPC
	RpcFirstBlock prometheus.Gauge
	RpcLastBlock  prometheus.Gauge

	// API
	ApiNumRequests prometheus.Gauge
}

// NewPromServer creates a new Prometheus server
func NewPromServer() *PromServer {
	server := PromServer{
		// PROCESS
		ProcChunkSize: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "chunk_size",
			Help: "Chunk size",
		}),
		ProcNumDecoders: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "num_decoders",
			Help: "Number of decoders",
		}),
		// Number processors
		ProcNumProcessors: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "num_processors",
			Help: "Number of processors",
		}),
		// Running since
		ProcRunningSince: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "proc_running_since",
			Help: "Timestamp when the process was started",
		}),
		// Query rate
		ProcNumQueried: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "proc_num_queried",
			Help: "Number of query results (rows)",
		}),
		// Total Num
		ProcNumDecoded: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "proc_num_decoded",
			Help: "Decoded entities (slots) over time period",
		}),
		// Total scanned
		ProcNumScanned: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "proc_num_scanned",
			Help: "Number of slots scanned during this session",
		}),
		// Total missed
		ProcNumMissed: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "proc_num_missed",
			Help: "Number of slots found missing during this session",
		}),
		// Total unknown
		ProcNumUnknown: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "proc_num_unknown",
			Help: "Number of slots found unknown during this session",
		}),
		// Total available
		ProcNumAvailable: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "proc_num_available",
			Help: "Number of slots found available during this session",
		}),

		// DATABASE
		// Max blockheight
		DbMaxSlot: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "db_max_slot",
			Help: "Highest slot number in local database",
		}),
		// Total blocks
		DbNumSlots: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "db_num_slot",
			Help: "Total number of slots in local database",
		}),
		// Total missed
		DbNumMissed: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "db_num_missed",
			Help: "Total number of missed slots in local database",
		}),
		// Total unknown
		DbNumUnknown: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "db_num_unknown",
			Help: "Total number of unknown slots in local database",
		}),
		// Total available
		DbNumAvailable: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "db_num_available",
			Help: "Total number of availabe slotss in local database",
		}),

		// RPC
		// First block
		RpcFirstBlock: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "rpc_first_block",
			Help: "First block available on the RPC node",
		}),
		// Last block
		RpcLastBlock: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "rpc_last_block",
			Help: "Last block available on the RPC node",
		}),

		// API
		// Num of request
		ApiNumRequests: promauto.NewGauge(prometheus.GaugeOpts{
			Name: "api_num_requests",
			Help: "Number of requests to the API",
		}),
	}

	r := prometheus.NewRegistry()
	r.MustRegister(
		server.ProcChunkSize,
		server.ProcNumDecoders,
		server.ProcNumProcessors,
		server.ProcRunningSince,
		server.ProcNumQueried,
		server.ProcNumDecoded,
		server.ProcNumScanned,
		server.ProcNumMissed,
		server.ProcNumUnknown,
		server.ProcNumAvailable,
		server.DbMaxSlot,
		server.DbNumSlots,
		server.DbNumMissed,
		server.DbNumUnknown,
		server.DbNumAvailable,
		server.RpcFirstBlock,
		server.RpcLastBlock,
		server.ApiNumRequests,
	)

	handler := promhttp.HandlerFor(r, promhttp.HandlerOpts{})
	http.Handle("/metrics", handler)

	return &server
}

// Serve starts the Prometheus server
func (server PromServer) serve() {
	err := http.ListenAndServe(":2112", nil)
	if err != nil {
		panic(err)
	}

}
