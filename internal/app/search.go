package app

import (
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/bigtable"
)

// Searcher Loop over the blocks found by the decoders and check for missing blocks
func Searcher(checker *BlockChecker, blockFound <-chan *DecodeResult, overview *StatsOverview) {
	db := checker.Db()
	missing := checker.missingSlots
	orphans := checker.orphanSlots
	stats := overview.Stats

	// Check if the parent block was already processed
	for result := range blockFound {
		block := result.block
		slotRange := result.slotRange

		// Do any parsing/indexing here <<<
		// In this case we'll be creating an index
		// to make sure we don't miss any blocks

		slotNumber := result.slot
		parentSlot := block.ParentSlot
		parentInRange := slotRange.Includes(parentSlot)

		// Check if this block was already referenced previously
		db.UpsertSlotParent(slotNumber, parentSlot)
		db.UpsertSlotState(slotNumber, bigtable.Available)

		if block.BlockHeight != nil {
			db.UpsertSlotBlock(slotNumber, block.BlockHeight.BlockHeight)
		}

		// Check if we have skipped slots in between
		if slotNumber-parentSlot > 1 && parentSlot > 0 {
			// We have skipped slots, so we mark them as skipped
			for i := parentSlot + 1; i < slotNumber; i++ {
				missing.Delete(i)
				orphans.Delete(i)
				db.UpsertSlotState(i, bigtable.Skipped)
			}
		}

		// Check if the block was already referenced previously
		if missing.Has(slotNumber) {
			// We remove it from the missing list
			missing.Delete(slotNumber)
		} else {
			// Otherwise we add it to the orphan list
			orphans.Add(slotNumber)
		}

		// Check if the parent block was already processed
		if orphans.Has(parentSlot) {
			// We found the parent block, so we remove it from the orphan list
			orphans.Delete(parentSlot)
			if parentInRange {
				db.UpsertSlotState(parentSlot, bigtable.Available)
			}
		} else if parentSlot != 0 {
			missing.Add(parentSlot)
			// If the parent slot was in the scan range, we mark it as missing
			if parentInRange {
				db.UpsertSlotState(parentSlot, bigtable.Missing)
			}
		}
		stats.ProcNumScanned.Add(1)
	}
}
