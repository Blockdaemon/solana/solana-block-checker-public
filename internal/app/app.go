package app

import (
	"fmt"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/api"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/bigtable"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/helpers"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/logger"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/rpc"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/sql"
	"sync"
	"sync/atomic"
	"time"
)

type BlockChecker struct {
	api.BlockCheckerIntf

	ChunkSize     uint64
	NumDecoders   uint64
	NumProcessors uint64

	rpc          *rpc.Rpc
	db           *sql.DatabaseClient
	bigtable     *bigtable.BigtableClient
	api          *api.Api
	stats        *StatsOverview
	orphanSlots  *SlotsMap
	missingSlots *SlotsMap
	slotRanges   *chan *helpers.SlotRange
	slotFound    *chan *DecodeResult
}

// SlotRanges return the slot ranges channel
func (b BlockChecker) SlotRanges() chan *helpers.SlotRange {
	return *b.slotRanges
}

// SlotFound return the slot found channel
func (b BlockChecker) SlotFound() chan *DecodeResult {
	return *b.slotFound
}

// NewBlockChecker creates a new BlockChecker and initializes all the components
func NewBlockChecker(chunkSize uint64, numDecoders uint64, numProcessors uint64, rpc *rpc.Rpc, db *sql.DatabaseClient, bigtable *bigtable.BigtableClient, api *api.Api, stats *StatsOverview) *BlockChecker {
	slotRanges := make(chan *helpers.SlotRange)
	slotFound := make(chan *DecodeResult)

	checker := BlockChecker{
		ChunkSize:     chunkSize,
		NumDecoders:   numDecoders,
		NumProcessors: numProcessors,
		rpc:           rpc,
		db:            db,
		bigtable:      bigtable,
		api:           api,
		stats:         stats,
		orphanSlots:   NewSlotsMap(),
		missingSlots:  NewSlotsMap(),
		slotRanges:    &slotRanges,
		slotFound:     &slotFound,
	}

	for w := 0; w < int(numDecoders); w++ {
		go Decoder(&checker, slotRanges, slotFound, stats)
	}

	for w := 0; w < int(numProcessors); w++ {
		go Searcher(&checker, slotFound, stats)
	}

	go db.Poll()

	return &checker
}

// Db Rpc returns the RPC client
func (b BlockChecker) Db() *sql.DatabaseClient {
	return b.db
}

// AddScanRange adds a scan range to the queue
func (b BlockChecker) AddScanRange(start uint64, end uint64) {
	log := logger.GetLogger()
	slotStart := start
	chunkSize := b.ChunkSize
	go func() {
		for {
			slotEnd := slotStart + chunkSize

			if slotEnd > end {
				slotEnd = end
			}
			if slotStart > end {
				break
			}

			*b.slotRanges <- &helpers.SlotRange{Start: slotStart, End: slotEnd}
			log.Info(fmt.Sprintf("Added ranges %v - %v", slotStart, slotEnd))

			time.Sleep(100 * time.Millisecond)

			slotStart += chunkSize
		}
	}()
}

type SlotsMap struct {
	values  sync.Map
	counter atomic.Int64
}

// NewSlotsMap creates a new SlotsMap
func NewSlotsMap() *SlotsMap {
	slots := SlotsMap{
		values:  sync.Map{},
		counter: atomic.Int64{},
	}
	return &slots
}

// Has returns true if the slot is in the map
func (slots *SlotsMap) Has(slotNr uint64) bool {
	_, found := slots.values.Load(slotNr)
	return found
}

// Add adds a slot to the map
func (slots *SlotsMap) Add(slotNr uint64) {
	slots.values.Store(slotNr, struct{}{})
	slots.counter.Add(1)
}

// Delete removes a slot from the map
func (slots *SlotsMap) Delete(slotNr uint64) {
	slots.values.Delete(slotNr)
	if slots.counter.Load() > 0 {
		slots.counter.Add(-1)
	}
}

// Count returns the number of slots in the map
func (slots *SlotsMap) Count() int64 {
	return slots.counter.Load()
}
