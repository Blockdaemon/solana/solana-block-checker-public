package app

import (
	googleBigtable "cloud.google.com/go/bigtable"
	"context"
	"github.com/golang/protobuf/proto"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/bigtable"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/helpers"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/pkg/generated"
	"strconv"
)

const (
	tableName = "blocks"
)

type Encodings int

const (
	ENCODE_PROTO Encodings = iota
	ENCODE_BIN
)

type DecodeResult struct {
	block     *generated.ConfirmedBlock
	slotRange *helpers.SlotRange
	encoding  Encodings
	slot      uint64
}

// Decoder This function is called by the main process to decode blocks
func Decoder(checker *BlockChecker, slotRanges <-chan *helpers.SlotRange, blockFound chan<- *DecodeResult, overview *StatsOverview) {
	tbl := checker.bigtable.Client.Open(tableName)
	ctx := context.Background()

	// Decode blocks
	for slotrange := range slotRanges {
		overview.Stats.ProcRateQueried.Add(slotrange.End - slotrange.Start)

		// Scan one extra so we can check for missing parent
		startSlot := slotrange.Start
		endSlot := slotrange.End

		// Read the blocks in this range from Bigtable
		blockRange := googleBigtable.NewRange(bigtable.SlotKey(startSlot), bigtable.SlotKey(endSlot))

		// Iterate over the blocks in this range
		btErr := tbl.ReadRows(ctx, blockRange, func(row googleBigtable.Row) bool {
			x := row["x"]
			for _, item := range x {
				var b []byte
				var block generated.ConfirmedBlock
				var encoding Encodings
				var err error
				if item.Column == "x:proto" {
					encoding = ENCODE_PROTO
					b, err = bigtable.BigtableCompression(item.Value[0]).Uncompress(item.Value[bigtable.CompressionHeaderSize:])
					if err != nil {
						panic(err)
					}
				} else if item.Column == "x:bin" {
					encoding = ENCODE_BIN
					b, err = helpers.ExternalBinToProto(item.Value, "solana-bigtable-decoder", "--hex")
					if err != nil {
						panic(err)
					}
				}

				if err := proto.Unmarshal(b, &block); err != nil {
					panic(err)
				}

				slotNumber, err := strconv.ParseUint(item.Row, 16, 64)
				if err != nil {
					panic("Could not read slot number")
				}

				// Send the block to the processor
				blockFound <- &DecodeResult{
					block:     &block,
					slot:      slotNumber,
					slotRange: slotrange,
					encoding:  encoding,
				}

				// Update stats
				if slotrange.Includes(slotNumber) {
					overview.Stats.ProcNumDecoded.Add(1)
				}

				break // Stop decoding this row if we found a block, which should be always...
			}

			return true
		})

		if btErr != nil {
			panic(btErr)
		}
	}
}
