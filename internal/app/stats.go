package app

import (
	"github.com/pterm/pterm"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/api"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/logger"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/rpc"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/sql"
	"sync/atomic"
	"time"
)

type StatsOverview struct {
	prom           *PromServer
	term           *TermStats
	Stats          *Stats
	exposeStatsCli *bool
}

type TermStats struct {
	defaultHeader string
	headerFormat  string
	barFormat     string
	header        string
	bar           string
	area          *pterm.AreaPrinter
}

type Stats struct {
	// PROCESS
	// Chunk size
	ProcChunkSize atomic.Uint32
	// Number decoders
	ProcNumDecoders atomic.Uint32
	// Number processors
	ProcNumProcessors atomic.Uint32
	// Total available
	ProcRunningSince atomic.Uint32
	// Total queried
	ProcRateQueried atomic.Uint64
	// Total decoded
	ProcNumDecoded atomic.Uint64
	// Total scanned
	ProcNumScanned atomic.Uint64
	// Total missed
	ProcNumMissed atomic.Uint64
	// Total unknown
	ProcNumUnknown atomic.Uint64
	// Total available
	ProcNumAvailable atomic.Uint64

	// DATABASE
	// Max blockheight
	DbMaxBlockheight atomic.Uint64
	// Total blocks
	DbNumBlocks atomic.Uint64
	// Total missed
	DbNumMissing atomic.Uint64
	// Total unknown
	DbNumUnknown atomic.Uint64
	// Total available
	DbNumAvailable atomic.Uint64

	// RPC
	// First block
	RpcFirstBlock atomic.Uint64
	// Last block
	RpcLastBlock atomic.Uint64

	// API
	// Num of request
	ApiNumRequests atomic.Uint64
}

// PollProc polls the processor for stats
func (s *Stats) PollProc(checker *BlockChecker) {
	go func() {
		for {
			s.ProcChunkSize.Store(uint32(checker.ChunkSize))
			s.ProcNumProcessors.Store(uint32(checker.NumProcessors))
			s.ProcNumDecoders.Store(uint32(checker.NumDecoders))
			s.ProcRunningSince.Store(uint32(time.Now().Unix()))
			s.ProcNumMissed.Store(uint64(checker.missingSlots.Count()))
			time.Sleep(5 * time.Minute)
		}
	}()
}

// PollDb polls the database for stats
func (s *Stats) PollDb(db *sql.DatabaseClient) {
	go func() {
		for {
			maxHeight := db.MaxSlot()
			numRows := db.CountSlots()
			numAvailable := db.CountAvailable()
			numMissing := db.CountMissing()
			numUnknown := db.CountUnknown()

			s.DbMaxBlockheight.Store(maxHeight)
			s.DbNumBlocks.Store(uint64(numRows))
			s.DbNumAvailable.Store(uint64(numAvailable))
			s.DbNumMissing.Store(uint64(numMissing))
			s.DbNumUnknown.Store(uint64(numUnknown))
			time.Sleep(3 * time.Second)
		}
	}()
}

// PollRpc polls the rpc for stats
func (s *Stats) PollRpc(client *rpc.Rpc) {
	go func() {
		for {
			firstBlock := client.GetFirstBlock()
			lastBlock := client.GetSlot()
			s.RpcFirstBlock.Store(firstBlock)
			s.RpcLastBlock.Store(lastBlock)
			time.Sleep(1 * time.Minute)
		}
	}()
}

// PollApi polls the api for stats
func (s *Stats) PollApi(server *api.Api) {
	go func() {
		for {
			s.ApiNumRequests.Store(server.ReqCount.Load())
			time.Sleep(10 * time.Second)
		}
	}()

}

// NewStatsOverview creates a new stats overview
func NewStatsOverview(exposeStatsCli *bool) *StatsOverview {
	display := StatsOverview{
		prom:           NewPromServer(),
		term:           NewTermStats(),
		exposeStatsCli: exposeStatsCli,
	}

	stats := Stats{}
	display.Stats = &stats

	go display.prom.serve()

	display.Refresh(5)

	return &display
}

// NewTermStats creates a new terminal stats
func NewTermStats() *TermStats {
	// Create a writer to output stats to terminal
	area, err := pterm.DefaultArea.Start()
	if err != nil {
		panic(err)
	}

	// Set formats for outputting stats
	stats := TermStats{
		defaultHeader: "Solana History Checker",
		headerFormat:  "Chunksize: %v\nDecoders: %v\nProcessors: %v\nRPC last block: %v\nDB last block: %v\nDifference: %v\nRunning since: %s",
		barFormat:     "DB last block: %d | Queried: %d | Decoded: %d | Processed: %d | Available: %d | Unknown: %d | Missing: %d | API req: %v",
		area:          area,
	}

	return &stats
}

// updateTerminal updates the terminal with the latest stats
func (overview *StatsOverview) updateTerminal() {
	term := overview.term
	update := overview.Stats

	defaultHeader := pterm.DefaultHeader.Sprintln(term.defaultHeader)
	term.generateHeader(update)
	term.generateBar(update)
	if *overview.exposeStatsCli {
		term.area.Update(defaultHeader + term.header + "\n" + term.bar)
	} else {
		logger.GetLogger().Info(term.bar)
	}
}

// updateProm updates the prometheus server with the latest stats
func (overview *StatsOverview) updateProm() {
	prom := overview.prom
	stat := overview.Stats

	prom.ProcChunkSize.Set(float64(stat.ProcChunkSize.Load()))
	prom.ProcNumDecoded.Set(float64(stat.ProcNumDecoded.Load()))
	prom.ProcChunkSize.Set(float64(stat.ProcChunkSize.Load()))
	prom.ProcNumDecoders.Set(float64(stat.ProcNumDecoders.Load()))
	prom.ProcNumProcessors.Set(float64(stat.ProcNumProcessors.Load()))
	prom.ProcRunningSince.Set(float64(stat.ProcRunningSince.Load()))
	prom.ProcNumQueried.Set(float64(stat.ProcRateQueried.Load()))
	prom.ProcNumDecoded.Set(float64(stat.ProcNumDecoded.Load()))
	prom.ProcNumScanned.Set(float64(stat.ProcNumScanned.Load()))
	prom.ProcNumMissed.Set(float64(stat.ProcNumMissed.Load()))
	prom.ProcNumUnknown.Set(float64(stat.ProcNumUnknown.Load()))
	prom.ProcNumAvailable.Set(float64(stat.ProcNumAvailable.Load()))
	prom.DbMaxSlot.Set(float64(stat.DbMaxBlockheight.Load()))
	prom.DbNumSlots.Set(float64(stat.DbNumBlocks.Load()))
	prom.DbNumMissed.Set(float64(stat.DbNumMissing.Load()))
	prom.DbNumUnknown.Set(float64(stat.DbNumUnknown.Load()))
	prom.DbNumAvailable.Set(float64(stat.DbNumAvailable.Load()))
	prom.RpcFirstBlock.Set(float64(stat.RpcFirstBlock.Load()))
	prom.RpcLastBlock.Set(float64(stat.RpcLastBlock.Load()))
	prom.ApiNumRequests.Set(float64(stat.ApiNumRequests.Load()))
}

// Refresh updates the stats every interval
func (overview *StatsOverview) Refresh(intervalSec int64) {
	go func() {
		for {
			overview.updateTerminal()
			overview.updateProm()
			time.Sleep(time.Duration(intervalSec) * time.Second)
		}
	}()
}

// generateHeader generates the header for the terminal
func (term *TermStats) generateHeader(stats *Stats) {
	since := time.Unix(int64(stats.ProcRunningSince.Load()), 0)

	term.header = pterm.Info.Sprintf(term.headerFormat,
		stats.ProcChunkSize.Load(),
		stats.ProcNumDecoders.Load(),
		stats.ProcNumProcessors.Load(),
		stats.RpcLastBlock.Load(),
		stats.DbMaxBlockheight.Load(),
		stats.RpcLastBlock.Load()-stats.DbMaxBlockheight.Load(),
		since.Format(time.RFC822),
	)
}

// generateBar generates the bar for the terminal
func (term *TermStats) generateBar(stats *Stats) {
	term.bar = pterm.Success.Sprintf(term.barFormat,
		stats.DbMaxBlockheight.Load(),
		stats.ProcRateQueried.Load(),
		stats.ProcNumDecoded.Load(),
		stats.ProcNumScanned.Load(),
		stats.DbNumAvailable.Load(),
		stats.DbNumUnknown.Load(),
		stats.DbNumMissing.Load(),
		stats.ApiNumRequests.Load(),
	)
}
