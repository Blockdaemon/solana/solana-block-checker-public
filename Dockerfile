
FROM --platform=linux/amd64 ghcr.io/streamingfast/firehose-solana:solana-bigtable-decoder as chain
# Start from the latest golang base image
FROM --platform=linux/amd64 golang:latest as builder

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy from external to local
COPY --from=chain /app/solana-bigtable-decoder /solana-bigtable-decoder

# Copy go mod and sum files
COPY go.mod ./

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# Copy the source from the current directory to the Working Directory inside the container
COPY . .

# Build the Go app
#RUN go build -o main main.go
# RUN CGO_ENABLED=1 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o blockchecker cmd/checker/checker.go
RUN CGO_ENABLED=1 GOOS=linux go build -o blockchecker cmd/checker/checker.go

######## Start a new stage from scratch #######
FROM --platform=linux/amd64 ubuntu:latest

# Install ca-certifactes
RUN apt update && apt install -y ca-certificates

# Change workdir
WORKDIR /root/

# Copy the Pre-built binary file from the previous stage
COPY --from=builder /app/blockchecker /root/


# Copy bigtable-decoder
COPY --from=builder /solana-bigtable-decoder /usr/local/bin/solana-bigtable-decoder
# Mark as executable and make symlinks
RUN chmod +x /usr/local/bin/solana-bigtable-decoder && ln -s /usr/local/bin/solana-bigtable-decoder /root/solana-bigtable-decoder && ln -s /usr/local/bin/solana-bigtable-decoder /solana-bigtable-decoder
# Command to run the executable
CMD ["./blockchecker"]
