package main

import (
	"context"
	"flag"
	"fmt"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/api"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/app"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/bigtable"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/helpers"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/logger"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/rpc"
	"go.blockdaemon.com/solana/solana-bigtable-checker-public/internal/sql"
	"time"
)

func main() {
	chunkSize := flag.Uint64("chunk-size", 1000, "Number of blocks to validate at once")
	numDecoders := flag.Uint64("num-decoders", 5, "Number of decoders to query BigTable and decode its content")
	numProcessors := flag.Uint64("num-processors", 2, "Number of processors to actually check the block's state")
	startSlot := flag.Uint64("start", 0, "Start slot to start scanning from")
	stopSlot := flag.Uint64("stop", 0, "Stop slot to stop scanning")
	projectName := flag.String("project-id", "solana-data-production", "BigTable project to use")
	instanceName := flag.String("instance-name", "solana-ledger", "Instance name to use. Default: solana-ledger")
	exposeStatsCli := flag.Bool("expose-stats-cli", true, "Expose stats refreshing on CLI, otherwise just as logging")

	flag.Parse() // Parse the flags

	// Install signal handlers.
	//onReload := make(chan os.Signal, 1)
	//signal.Notify(onReload, syscall.SIGHUP)
	//ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	//defer cancel()

	log := logger.GetLogger()

	// Init all the different components
	ctx := context.Background()
	rpcClient := rpc.NewClient()
	db := sql.NewDatabaseClient()
	overview := app.NewStatsOverview(exposeStatsCli)
	apiServer := api.NewApi(ctx)
	bigtableClient := bigtable.NewBigtableClient(*projectName, *instanceName)

	// Init main app, glue components together
	checker := app.NewBlockChecker(
		*chunkSize,
		*numDecoders,
		*numProcessors,
		rpcClient,
		db,
		bigtableClient,
		apiServer,
		overview,
	)

	// Init stats
	stats := overview.Stats
	stats.PollProc(checker)
	stats.PollDb(db)
	stats.PollRpc(rpcClient)
	stats.PollApi(apiServer)

	// Init API
	go apiServer.Serve(checker)

	// Fill queue with slots to scan
	rangeStart := uint64(0)
	if *startSlot > 0 {
		rangeStart = *startSlot
		log.Info(fmt.Sprintf("Starting first block from cli arg: %v", rangeStart))
	} else {
		rangeStart = db.LastSlot()
		rescanMargin := *chunkSize * *numDecoders
		if rescanMargin > rangeStart {
			rangeStart = 0
		} else {
			log.Info(fmt.Sprintf("Starting with last block from db %v minus rescan %v", rangeStart, rescanMargin))
			rangeStart -= rescanMargin
		}
	}

	rpcLastSlot := rpcClient.GetSlot()

	// Add all slot ranges as chunks to queue for decoders and processors taking into account the RPC last block
	for {
		// If we finished all requests slots, wait a bit for finish and then exit
		// If no stop-slot, we just keep running
		if *stopSlot > 0 && rangeStart >= *stopSlot {
			log.Info("Finished ranges")
			time.Sleep(600 * time.Second)
			break
		}

		// Adjust rangeEnd if we are passed the maximum (stop or rpc)
		rangeEnd := rpcLastSlot
		if *stopSlot > 0 {
			rangeEnd = helpers.Min(*stopSlot, rpcLastSlot)
		}

		// If we hit the latest slot on the network, wait for new slots to check
		if rangeStart >= rpcLastSlot || rangeStart >= rangeEnd {
			rpcLastSlot = rpcClient.GetSlot()
			time.Sleep(60 * time.Minute)
			continue
		}

		// If we have at least one chunk size, or we are hitting the leftovers till stop-slot, add the ranges
		if rangeEnd >= rangeStart || *stopSlot > 0 {
			checker.AddScanRange(rangeStart, rangeEnd)
			rangeStart = rangeEnd + 1
		}

		// Let's not go crazy on this
		time.Sleep(100 * time.Millisecond)
	}
}
