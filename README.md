# Solana BigTable Checker

## About
Blockdaemon created this tool to traverse the ledger by querying blocks in Bigtable and parsing it to the protobuf definition. In this case the blocks are parsed to check if there's any parent/child block missing, but any processing/indexing could be implemented additionally.

# Components
In a high level overview the application uses these components:

- Golang application: implementation for logic of reading, decoding, parsing and storing blockchain information. It leverages Goroutines for non-blocking processing and maximizing performance
-
- Bigtable: storage for the ledger data in encoded format

- Solana RPC endpoint: getting the most recent blockheight (we won’t need to scan passed that, because it’ll obviously be missing in bigtable).

- Solana bigtable decoder: an external tool to decode the bigtable data into a Go-struct. This only applies for the blocks that are saved in binary format.

- Protobuf definition: to decode the blocks that are encoded using protobuf, we generated the block-model with protoc.

- SQLite: once data is decoded and parsed, we are saving the block state into SQLite (available, missing, unknown)

- API: The application exposes an API to retrieve block-state information and also to initiate a re-scan of a particular slotrange. The API specification is exposes in Swagger/OpenAPI format.

- Prometheus + Grafana: The application exposes a Prometheus endpoint so we can monitor stats on progress and results in a Grafana dashboard.

# Application
The application’s main task is to bring all the different components together in a combined process. Some logging has been implemented to indicate if the application is operational as it should. The application splits functionalities into different Goroutines, so that none will block the others.

The basic flow for the application is:

- Initiate all basic components like logging,  context/sig handling, parsing flags
- Initiate all external components required and bind them together in the BlockChecker-struct.
- Connect to Bigtable, open/create SQLite database, connect to Solana’s RPC endpoint
- Expose API (port 8403, docs on path /swagger) and Prometheus endpoints (port 2112 path /metrics)
- Initiate workers depending on configuration. To make processing fast, processing the ledger was implemented in a sync way like ‘workers’-concept. Each worker will take message (“jobs”) from the channel (“queue”) and process this. The number of workers can be set by passing flags when calling the binary. There’s “decoders” (query bigtable, decode payload) and “searchers” (connect blocks, save state).
- Determine scan range, defaulting to the latest slot in database minus (num searchers * chunksize)
- Add all ranges (depending on chunksize). While ranges are being added, workers start processing the reqests. Max is the last block on the rpc client. If this is reached, the application will sleep waiting for new blocks. Meanwhile
- Stats are being collected on a 5 seconds interval. This includes stats from processing, database, rpc and API. These stats can be exposed on CLI or logging and also thru Prometheus (enabled by default).
- API is ready to take requests. It’ll use the SQLite data to provide realtime responses. Calling the rescan endpoint will add more slotranges (chunked to chunksize) to the workers queue.
- Flush blockstate to SQLite database on a 5 seconds interval. All blockstate changes are collected in a sync.Map to collectively update the database. This is to minimize write-overhead as it would potentially be doing thousands per second.

There’s still some improvement to be made in catching errors, as many errors are handled as panics. This is to make debugging easier and be sure any malfunction is detected. In some cases, like RPC timeouts, it would be nice to just retry instead of crashing the application.

## Bigtable
Obviously you'll need to run your own Bigtable cluster storing the complete ledger for Solana mainnet. Warehouse nodes needs to be continuously uploading new data. This application makes sure this data is consistent.

## RPC endpoint
The RPC endpoint is used to retrieve the current ledger’s blockheight. By default localhost is used, but it could be any endpoint to be configured in prod_config.json. The first and last block are exposed as Prometheus stats

## Decoding
Ledger data is saved encoded in Bigtable as either binary (bin) or Protobuf (proto). This is indicated by the column x:bin or x:proto:

## Binary decoder
For decoding binary blocks in Go we could not find a library. There’s a binary available though that can decode the payload and parse it as a Go-struct. This probably comes with a cost of overhead, but was the best solution we could come up with. The parallelism should solve the bottleneck from calling an external binary.

## Protobuf decoder
Blocks that are encoded as proto can be easily decoded by unmarshalling these into a protoc-generated model. Generating the model ConfirmedBlock from pkg/confirmed_block.proto using protoc provides everything we need.

## SQLite
All results from processing the ledger info will be saved into a SQLite database (consisting of a single table). This data is later used to validate results or backfill missing blocks.

The structure for the database is as follows:

```sqlite
CREATE TABLE IF NOT EXISTS solanahistory (

blockheight INTEGER NOT NULL PRIMARY KEY,

state INTEGER NOT NULL

);
```
It could be useful to later also add a column for parent but this was out of scope for now.

## API
Block state data is exposed over an API. Currently the API has not implemented any limits neither does it support pagination (so handle with care).
Available endpoints include querying for block state, updating state or initializing rescanning a range of blocks.

Documentation is generated and available on http://localhost:8403/swagger when running this service

## Prometheus / Grafana
Exposing processing and database statistics to provide a operational dashboard in Grafana. The following metrics are exposed on localhost:2112/metrics:

| Metric | Description |
| ------ | ----------- |
| api_num_requests | Number of requests to the API |
| chunk_size | Chunk size |
| db_max_blockheight | Highest block number in local database |
| db_num_available | Total number of available blocks in local database |
| db_num_blocks | Total number of blocks in local database |
| db_num_missed | Total number of missed blocks in local database |
| db_num_unknown | Total number of unknown blocks in local database |
| num_decoders | Number of decoders |
| num_processors | Number of processors |
| proc_num_available | Number of blocks found available during this session |
| proc_num_decoded | Decoded entities (blocks) over time period |
| proc_num_missed | Number of blocks found missing during this session |
| proc_num_queried | Number of blocks scanned during this session |
| proc_num_unknown | Number of blocks found unknown during this session |
| proc_running_since | Timestamp when the process was started |
| rpc_first_block | First block available on the RPC node |
| rpc_last_block | Last block available on the RPC node |

# Usage
The application provides a few flags to configure soms of the behavior. More information is available by using the --help flag:

Usage of checker:

| Flag | Description                                                              |
| ---- |--------------------------------------------------------------------------|
| -chunk-size uint | Number of slots to validate at once (default 1000)                      |
| -expose-stats-cli | Expose stats refreshing on CLI, otherwise just as logging (default true) |
| -instance-name string | Instance name to use. Default: solana-ledger (default "solana-ledger")   |
| -num-decoders uint | Number of decoders to query BigTable and decode its content (default 5)  |
| -num-processors uint | Number of processors to actually check the block's state (default 2)     |
| -project-id string | BigTable project to use (default "solana-data-production")               |
| -start uint | Start slot to start scanning from                                        |

Make sure you have a file called credentials.json with the authentication key of your bigtable instance.

For example:
```
./blockchecker --chunk-size 100 --num-decoders 3 --num-processors 3
```
This will start the blockchecker parsing chunks of 100 slots with 2 decoders (threads) unserializing the data and 3 processors (threads) implementing the logic (check for missing slots, index data, whatever is customized in source code).

## Running SBC locally
Copy sample_config.json to prod_config.json and edit the file.
Run the app by running:
- `go mod tidy`
- `go run .`

Optionally one can run the Docker image by building from the Dockerfile
